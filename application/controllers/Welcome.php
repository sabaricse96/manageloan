<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()

	{
	  parent::__construct();
	  $this->load->model('Loan_model');
	  date_default_timezone_set('UTC');
	}
	public function index()
	{
		$this->load->view('landing');
	}

	public function addgroup(){
		$this->load->view('addgroup');
	}


	function generategroupnumber()
  {
    
        $query = 'SELECT COUNT(id) AS TOTCOUNT FROM GroupMaster';
        $exquery = $this->db->query($query);
        if ($exquery->num_rows() > 0)
        {
          $row = $exquery->result_array();
          $totcount = $row[0]['TOTCOUNT'];
        }
        else
        {
          $totcount = 0;
        }
        $newno = $totcount + 1;
        $newgroupno = 'GRP' . sprintf('%05s', $newno);
        return $newgroupno;
  }


  function SaveGroup(){
	$grpid =  $this->generategroupnumber();
	//   echo "<pre>"; print_r($_POST); echo "</pre>";
	if(isset($_POST['groupname']) && $_POST['groupname']!=""){
		$grpname = $_POST['groupname'];
	}else{
		$grpname = "";
	}

	if(isset($_POST['noofapplicant']) && $_POST['noofapplicant']!=""){
		$noofapplicant = $_POST['noofapplicant'];
	}else{
		$noofapplicant = "";
	}

	if(isset($_POST['loanamount']) && $_POST['loanamount']!=""){
		$loanamnt = $_POST['loanamount'];
	}else{
		$loanamnt = "";
	}
	$DataArray = array(
		'groupid'          => $grpid,
		'groupname'        => $grpname,
		'applicantcount'   => $noofapplicant,
		'loan_amt'         => $loanamnt
	);

	$InsertGroupData = $this->Loan_model->InsertGroupData($grpname,$DataArray);
	if(isset($InsertGroupData) && $InsertGroupData['status']=="1"){
		$this->session->set_flashdata("success",$InsertGroupData['description']);
	}else if(isset($InsertGroupData) && $InsertGroupData['status']=="0"){
		$this->session->set_flashdata("danger",$InsertGroupData['description']);
	}else{
		$this->session->set_flashdata("warning",$InsertGroupData['description']);
	}

	redirect('Welcome/addgroup','refresh');	  
  }
}
