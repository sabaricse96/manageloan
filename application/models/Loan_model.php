<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_model extends CI_Model {

    public function InsertGroupData($grpname,$data){
        $this->db->select('*');
        $this->db->from('GroupMaster');
        $this->db->where('groupname',$grpname);
        $xqry = $this->db->get();
        if(isset($xqry) && $xqry->num_rows()=="0"){
            $xqry1 = $this->db->insert('GroupMaster',$data);
            if(isset($xqry1) && $xqry1=="1"){
                $data['status']      = 1;
                $data['description'] = "Group Created Successfully";
            }else{
                $data['status']      = 0;
                $data['description'] = "Database Error";
            }
        }else{
            $data['status']      = 2;
            $data['description'] = "Record Already Found";
        }

        return $data;
    }
    
}
