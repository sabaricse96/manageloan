<div class="col-sm-8 col-offset-1">
<?php if(isset($_SESSION['success']) && $_SESSION['success']!=""){ ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                <?php echo $_SESSION['success']; ?>
              </div>
        <?php }elseif(isset($_SESSION['warning']) && isset($_SESSION['warning'])!=""){ ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                <?php echo $_SESSION['warning']; ?>
              </div>
        <?php }elseif(isset($_SESSION['danger']) && isset($_SESSION['danger'])!=""){ ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                <?php echo $_SESSION['danger']; ?>
              </div>
        <?php } ?>
</div>