<?php $this->load->view('top'); ?> 
<div class="wrapper">
<?php $this->load->view('header'); ?> 
<?php $this->load->view('leftmenu'); ?> 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <br>
    <!-- Division for row -->
    <div class="row container">
        <!-- Content for Form Division -->
        <?php $this->load->view('alert'); ?>
    <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Loan Group</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url('Welcome/SaveGroup'); ?>" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Groupname</label>
                  <input class="form-control" id="groupname" name="groupname" placeholder="Group Name" type="text">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">No of Applicant</label>
                  <input class="form-control" id="noofapplicant" name="noofapplicant" placeholder="Enter in Number" type="text">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Loan Amount</label>
                  <input class="form-control" id="loanamount" name="loanamount" placeholder="&#8377; 100000" type="text">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
    <!-- Form Division Ending -->
    </div>
    <!-- Division for row Ending -->
    

</div>
<!-- ./wrapper -->

<?php $this->load->view('bottom'); ?>